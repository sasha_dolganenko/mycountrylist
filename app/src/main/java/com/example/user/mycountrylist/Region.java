package com.example.user.mycountrylist;

public class Region implements Comparable<Region>{
    public String region;

    public Region(String region) {
        this.region = region;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    @Override
    public boolean equals(Object object) {
        if (object instanceof Region)
            return region.equals(((Region) object).region);
        return false;
    }

    @Override
    public int hashCode() {
        return region.hashCode();
    }

    @Override
    public int compareTo(Region region) {
        return this.region.compareTo(region.region);
    }
}
