package com.example.user.mycountrylist;

public class Subregion {
    public String name;
    public String fromRegion;


    public Subregion(String fromRegion, String name) {
        this.fromRegion = fromRegion;
        this.name = name;
    }

    public String getFromRegion() {
        return fromRegion;
    }

    public String getName() {
        return name;
    }
}
