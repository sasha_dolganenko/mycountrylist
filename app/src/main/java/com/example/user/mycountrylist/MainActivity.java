package com.example.user.mycountrylist;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends AppCompatActivity {
    private Region[] regions;
    private ListView listView;
    public static final String KEY_REGION = "region";
    private List<Map<String, Object>> adapterData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final Toast noConnection = Toast.makeText(getApplicationContext(),
                "Cant load data", Toast.LENGTH_SHORT);
        setTitle("All regions");
        listView = (ListView) findViewById(R.id.region_list_view);
        RetrofitRegion.getRegions(new Callback<List<Region>>() {
            @Override
            public void success(List<Region> regions, Response response) {
                getAllRegions(regions);
                fillCountryList();
            }

            @Override
            public void failure(RetrofitError error) {
                noConnection.show();
            }
        });

        listView.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                        Intent intent = new Intent(MainActivity.this, CountryListActivity.class);
                        intent.putExtra(
                                KEY_REGION,
                                adapterData.get(position).get(KEY_REGION).toString());
                        startActivity(intent);
                    }
                }
        );
    }

    private void getAllRegions(List<Region> regions) {
        Set<Region> uniqueRegions = new HashSet<>();
        for (Region region : regions) {
            if (region.getRegion().trim().length() > 0) {
                uniqueRegions.add(region);
            }
        }
        this.regions = new Region[uniqueRegions.size()];
        uniqueRegions.toArray(this.regions);
        Arrays.sort(this.regions);
    }

    private void fillCountryList() {
        adapterData = new ArrayList<>();
        for (Region region : regions) {
            Map<String, Object> data = new HashMap<>();
            data.put(KEY_REGION, region.getRegion());
            adapterData.add(data);
        }
        listView.setAdapter(
                new SimpleAdapter(
                        this,
                        adapterData,
                        R.layout.region_in_list,
                        new String[]
                                {
                                        KEY_REGION,
                                },
                        new int[]
                                {
                                        R.id.region_in_list_text_view,
                                })
        );
    }
}
