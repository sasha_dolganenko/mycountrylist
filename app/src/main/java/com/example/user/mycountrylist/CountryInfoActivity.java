package com.example.user.mycountrylist;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class CountryInfoActivity extends AppCompatActivity {
    private Button homeButton;
    private TextView countryNameTextView;
    private TextView capital;
    private TextView language;
    private TextView population;
    private Country country;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.country_info);
        country = (Country) getIntent().getSerializableExtra(CountryListActivity.KEY_COUNTRY);
        ActionBar actionBar = getSupportActionBar();
        try
        {
            actionBar.setDisplayHomeAsUpEnabled( true );
        }
        catch ( Throwable ignore )
        {
        }

        countryNameTextView = (TextView) findViewById(R.id.country_text_view);
        capital = (TextView) findViewById(R.id.capital_text_view);
        language = (TextView) findViewById(R.id.language_text_view);
        population = (TextView) findViewById(R.id.population_text_view);

        countryNameTextView.setText(country.getName());
        capital.setText("Capital : " + country.getCapital());
        language.setText("Languages : " + country.getLanguagesList());
        population.setText("Population : " + Integer.toString(country.getPopulation()) + "p.");

    }

    @Override
    public boolean onOptionsItemSelected( MenuItem item )
    {
        if ( item.getItemId() == android.R.id.home )
        {
            onBackPressed();
            return true;
        }
        return false;
    }
}
