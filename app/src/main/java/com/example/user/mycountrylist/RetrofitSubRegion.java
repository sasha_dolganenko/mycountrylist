package com.example.user.mycountrylist;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.http.GET;

public class RetrofitSubRegion {
    public static final String ENDPOINT = "http://restcountries.eu/rest";
    private static ApiInterface apiInterface;

    interface ApiInterface {
        @GET("/v1/all")
        void getSubRegions(Callback<List<Subregion>> callback);
    }
    static {
        init();
    }

    private static void init() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(ENDPOINT)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        apiInterface = restAdapter.create(ApiInterface.class);

    }


    public static void getSubRegions(Callback<List<Subregion>> callback) {
        apiInterface.getSubRegions(callback);
    }
}
