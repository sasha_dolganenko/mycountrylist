//package com.example.user.mycountrylist;
//
//import android.content.Intent;
//import android.os.Bundle;
//import android.support.v7.app.AppCompatActivity;
//import android.view.View;
//import android.widget.AdapterView;
//import android.widget.ListView;
//import android.widget.SimpleAdapter;
//
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.HashMap;
//import java.util.HashSet;
//import java.util.List;
//import java.util.Map;
//import java.util.Set;
//
//import retrofit.Callback;
//import retrofit.RetrofitError;
//import retrofit.client.Response;
//
//public class SubregionActivity extends AppCompatActivity {
//    public static final String KEY_SUBREGION = "subregion";
//    private ListView subregionList;
//    private Subregion[] subregions;
//    private List<Map<String, Object>> adapterData;
//
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.sub_region_list);
//        setTitle("Subregions");
//        subregionList = (ListView) findViewById(R.id.sub_region_list_view);
//        RetrofitSubRegion.getSubRegions(new Callback<List<Subregion>>() {
//            @Override
//            public void success(List<Subregion> subregions, Response response) {
//                getAllSubRegions(subregions);
//                fillSubregionList();
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//
//            }
//        });
//
//
//        subregionList.setOnItemClickListener(
//                new AdapterView.OnItemClickListener() {
//                    @Override
//                    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
//                        Intent intent = new Intent(SubregionActivity.this, CountryListActivity.class);
//                        intent.putExtra(
//                                KEY_SUBREGION,
//                                adapterData.get(position).get(KEY_SUBREGION).toString());
//                        startActivity(intent);
//                    }
//                }
//        );
//
//    }
//
//    private void getAllSubRegions(List<Subregion> subregions) {
//        Set<Subregion> uniqueSubregions = new HashSet<>();
//        for (Subregion subregion : subregions) {
//            if (subregion.getFromRegion().trim().length() > 0) {
//                subregions.add(subregion);
//            }
//        }
//        this.subregions = new Subregion[uniqueSubregions.size()];
//        uniqueSubregions.toArray(this.subregions);
//        Arrays.sort(this.subregions);
//
//    }
//
//    private void fillSubregionList() {
//        adapterData = new ArrayList<>();
//        for (Subregion subregion : subregions) {
//            Map<String, Object> data = new HashMap<>();
//            data.put(KEY_SUBREGION, subregion.getFromRegion());
//            adapterData.add(data);
//        }
//        subregionList.setAdapter(
//                new SimpleAdapter(
//                        this,
//                        adapterData,
//                        R.layout.subregion_in_list,
//                        new String[]
//                                {
//                                        KEY_SUBREGION,
//                                },
//                        new int[]
//                                {
//                                        R.id.subregion_in_list_text_view,
//                                })
//        );
//
//    }
//}
//
