package com.example.user.mycountrylist;


import android.content.Context;

import java.util.List;


import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.http.GET;

public class RetrofitRegion {
    public static final String ENDPOINT = "http://restcountries.eu/rest";
    private static ApiInterface apiInterface;

    interface ApiInterface {
        @GET("/v1/all")
        void getRegions(Callback<List<Region>> callback);
    }

    static {
        init();
    }

    private static void init() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(ENDPOINT)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        apiInterface = restAdapter.create(ApiInterface.class);
    }

    public static void getRegions(Callback<List<Region>> callback) {
        apiInterface.getRegions(callback);
    }

}
