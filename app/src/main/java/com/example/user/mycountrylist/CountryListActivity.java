package com.example.user.mycountrylist;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class CountryListActivity extends AppCompatActivity {
    private EditText search;
    private ImageButton deleteTextButton;

    private Timer timer;
    private TimerTask textFilterTimerTask;
    private Handler handler;

    public static final int FILTER_DELAY = 500;

    private String region;

    public static final String KEY_COUNTRY = "country";
    public static final String KEY_COUNTRY_NAME = "country_name";

    private List<Country> countries;
    private ListView countryListView;
    private List<Map<String, Object>> adapterData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.country_list);
        final Toast noConnection = Toast.makeText(getApplicationContext(),
                "Can't load data!", Toast.LENGTH_SHORT);
        timer = new Timer();
        handler = new Handler();
        search = (EditText) findViewById(R.id.search_edit_text);
        deleteTextButton = (ImageButton) findViewById(R.id.button_clear_text);


        ActionBar actionBar = getSupportActionBar();
        try {
            actionBar.setDisplayHomeAsUpEnabled(true);
        } catch (Throwable ignore) {
        }
        region = getIntent().getStringExtra(MainActivity.KEY_REGION);
        setTitle(region);

        countryListView = (ListView) findViewById(R.id.country_list_view);
        RetrofitCountry.getCountries(new Callback<List<Country>>() {
            @Override
            public void success(List<Country> countries, Response response) {
                getAllCountriesForCurrentRegion(countries);
                fillList();
            }

            @Override
            public void failure(RetrofitError error) {
                noConnection.show();
            }
        });


        deleteTextButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        search.setText("");
                    }
                }
        );

        countryListView.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                        Intent intent = new Intent(CountryListActivity.this, CountryInfoActivity.class);
                        Country country = (Country) adapterData.get(position).get(KEY_COUNTRY);
                        intent.putExtra(KEY_COUNTRY, country);
                        startActivity(intent);
                    }
                }
        );

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                startFilterTimer();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void getAllCountriesForCurrentRegion(List<Country> countries) {
        this.countries = new ArrayList<>();
        for (Country country : countries) {
            if (country.getName().trim().length() > 0 &&
                    country.getRegion().equalsIgnoreCase(region)) {
                this.countries.add(country);
            }
        }
        int numberOfCountries = this.countries.size();
        setTitle(region + " - " + numberOfCountries);
        Collections.sort(this.countries);
    }

    private void fillList() {
        adapterData = new ArrayList<>();
        for (Country country : countries) {
            String searchString = search.getText().toString().toLowerCase();
            if (searchString.length() == 0 || country.name.toLowerCase().contains(searchString)) {
                Map<String, Object> data = new HashMap<>();
                data.put(KEY_COUNTRY, country);
                data.put(KEY_COUNTRY_NAME, country.getName());
                adapterData.add(data);
            }
        }
        countryListView.setAdapter(
                new SimpleAdapter(
                        this,
                        adapterData,
                        R.layout.country_in_list,
                        new String[]
                                {
                                        KEY_COUNTRY_NAME,
                                },
                        new int[]
                                {
                                        R.id.country_in_list_text_view,
                                })
                {
                    @Override
                    public View getView(int position, View convertView, ViewGroup parent) {
                        View view = super.getView(position, convertView, parent);
                        ImageView flagImageView =
                                (ImageView) view.findViewById(R.id.image_view_flag);
                        flagImageView.setImageResource(R.mipmap.ic_launcher);
                        return view;
                    }
                });
    }

    private synchronized void startFilterTimer() {
        stopFilterTimer();
        textFilterTimerTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(
                        new Runnable() {
                            @Override
                            public void run() {
                                fillList();
                            }
                        });
            }
        };
        timer.schedule(textFilterTimerTask, FILTER_DELAY);
    }

    private synchronized void stopFilterTimer() {
        if (textFilterTimerTask != null) {
            textFilterTimerTask.cancel();
            textFilterTimerTask = null;
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return false;
    }

}


