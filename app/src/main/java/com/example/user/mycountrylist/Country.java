package com.example.user.mycountrylist;

import java.io.Serializable;
import java.util.List;
import java.util.Locale;

public class Country implements Comparable<Country>, Serializable {
    public String name;
    public String capital;
    public List<String> languages;
    private String region;
    public int population;


    public Country(String capital, List<String> languages, String name, String region, int population) {
        this.capital = capital;
        this.languages = languages;
        this.name = name;
        this.region = region;
        this.population = population;
    }


    public List<String> getLanguages() {
        return languages;
    }

    public String getLanguagesList() {
        String languagesList = "";
        for (String language : languages) {
            Locale locale = new Locale(language);
            languagesList += (languagesList.isEmpty() ? "" : ", ") + locale.getDisplayName();
        }
        return languagesList;
    }

    public String getName() {
        return name;
    }


    public int getPopulation() {
        return population;
    }


    public String getRegion() {
        return region;
    }

    public String getCapital() {
        return capital;
    }

    @Override
    public int compareTo(Country country) {
        return this.name.compareTo(country.name);
    }
}
